import ReactDOM from 'react-dom/client';
import App from './App';
import AuthStore from './utilities/auth/store';
import { createContext } from 'react';

interface State {
  store: AuthStore,
}

const store = new AuthStore()

export const Context = createContext<State>({ store, })

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <Context.Provider value={{ store }}>
    <App />
  </Context.Provider>
);
