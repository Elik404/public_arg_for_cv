import "./scss/main.scss";

import { FC } from "react";
import { Route, Routes, BrowserRouter } from "react-router-dom";
import PrivateUserRoute from "./utilities/auth/PrivateUserRoute";

import Authorization from "./components/Authorization";

import { observer } from "mobx-react-lite";
import AdministratorPanel from "./components/AdministratorPanel";
import PrivateAdminRoute from "./utilities/auth/PrivateAdminRoute";
import AddUser from "./components/AddUser";
import FileAddPanel from "./components/FileAddPanel";
import Users from "./components/Users";
import EditUser from "./components/EditUser";
import FormTable from "./components/FormTable";
import FileEditPanel from "./components/FileEditPanel";
import Litigation from "./components/Litigation";
import LitigationPhotos from "./components/LitigationPhotos";
import AddLitigation from "./components/AddLitigation";
import EditLitigation from "./components/EditLitigation";
import Litigations from "./components/Litigations";
import cookiesService from "./utilities/storages/CookiesService";



const App: FC = () => {

  // const { store } = useContext(Context)
  // useEffect(() => {
  //   console.log('APP checkAuth')
  //   if (localStorage.getItem('token')) {
  //     store.checkAuth()
  //   }
  // }, [store])

  const isAdmin = Boolean(cookiesService.getCookie('isAdmin'));

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/login" element={<Authorization />} />
        {isAdmin ?
          <Route path="/" element={<PrivateAdminRoute />}>
            <Route path={`admin`} element={<AdministratorPanel />} />
            <Route path={`admin/add-user`} element={<AddUser />} />
            <Route path={`admin/users`} element={<Users />} />
            <Route path={`admin/edit-user`} element={<EditUser />} />
            <Route path={``} element={<Litigations />} />
            <Route path={`add-case`} element={<AddLitigation />} />
            <Route path={`folder`} element={<Litigation />} />
            <Route path={`folder/edit`} element={<EditLitigation />} />
            <Route path={`folder/photos`} element={<LitigationPhotos />} />
            <Route path={`folder/photos/add`} element={<FileAddPanel />} />
            <Route path={`folder/photos/edit`} element={<FileEditPanel />} />
            <Route path={`folder/form-table`} element={<FormTable />} />
          </Route>
          :
          <Route path="/" element={<PrivateUserRoute />}>
            <Route path="" element={<Litigations />} />
            <Route path={`add-case`} element={<AddLitigation />} />
            <Route path={`folder`} element={<Litigation />} />
            <Route path={`folder/edit`} element={<EditLitigation />} />
            <Route path={`folder/photos`} element={<LitigationPhotos />} />
            <Route path={`folder/photos/add`} element={<FileAddPanel />} />
            <Route path={`folder/photos/edit`} element={<FileEditPanel />} />
            <Route path={`folder/form-table`} element={<FormTable />} />
          </Route>

        }
        <Route path="*" element={<div>404... not found </div>} />
      </Routes>
    </BrowserRouter>
  );
}

export default observer(App);
