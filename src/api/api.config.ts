import { IAuthData } from "../models/authorization/IAuthData";
import cookiesService from "../utilities/storages/CookiesService";
import authService from "./api.auth.service";

export const API_URL = process.env.REACT_APP_API_URL!
// export const API_URL = `http://***` // for development

export const TOKEN_AUTH = `Bearer ${localStorage.getItem("token")}`

class ApiService {
    private static instance: ApiService;

    private constructor() { }

    public static getInstance(): ApiService {
        if (!ApiService.instance) {
            ApiService.instance = new ApiService();
        }
        return ApiService.instance;
    }

    async fetch(url: string, options: RequestInit = {}): Promise<Response> {
        const response = await fetch(url, options)

        if (response.status === 401) {
            const refreshToken = cookiesService.getCookie('refreshToken')
            const responseTokens = await authService.getTokens(refreshToken)
            if (responseTokens.status >= 400) {
                return response
            }

            const dataTokens: IAuthData = await responseTokens.json()
            localStorage.setItem("token", dataTokens.access_token);

            options.headers = {
                ...options.headers,
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }

            return await fetch(url, options);
        }
        return response;
    }
}

export default ApiService.getInstance();



/** РЕШЕНИЕ ЧЕРЕЗ БИБЛИОТЕКУ AXIOS  */

// export const instance = axios.create({
//   // withCredentials: true, // к запросу будет прикрепляться cookies
//   baseURL: API_URL,
// });


// instance.interceptors.response.use(
//   // в случае валидного accessToken ничего не делаем:
//   (config) => {
//     return config;
//   },
//   // в случае просроченного accessToken пытаемся его обновить:
//   async (error) => {
//    // предотвращаем зацикленный запрос, добавляя свойство _isRetry
//    const originalRequest = {...error.config};
//    originalRequest._isRetry = true;
//     if (
//       // проверим, что ошибка именно из-за невалидного accessToken
//       error.response.status === 401 &&
//       // проверим, что запрос не повторный
//       error.config &&
//       !error.config._isRetry
//     ) {
//       try {
//         // запрос на обновление токенов
//         const refreshToken = document.cookie.replace(/(?:(?:^|.*;\s*)refresh_token\s*=\s*([^;]*).*$)|^.*$/, "$1");
//         const resp = await axios.get<AuthResponse>(`${API_URL}:3111/gateway-api-backend/auth/get-tokens?grantType=refreshToken&refreshToken=${refreshToken}`);
//         // сохраняем новый accessToken в localStorage
//         localStorage.setItem("token", resp.data.access_token);
//         // переотправляем запрос с обновленным accessToken
//         return instance.request(originalRequest);
//       } catch (error) {
//         console.log(`AUTH ERROR ${error}`);
//       }
//     }
//     // на случай, если возникла другая ошибка (не связанная с авторизацией)
//     // пробросим эту ошибку
//     throw error;
//   }
// );