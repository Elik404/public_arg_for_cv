import { ILitigationData } from "../models/litigations/ILitigationData";
import { ILitigationUploadData } from "../models/litigations/ILitigationUploadData";
import { API_URL, TOKEN_AUTH } from "./api.config";
import api from "./api.config";

const LITIGATION_URL = `:3211/litigation-backend/api/litigations`

interface ILitigationService {
    getLitigation(litigationId: number): Promise<ILitigationData>;
    updateLitigation(litigationId: number, newNumber: string, newTemporaryNumber: string): Promise<Response>;
    deleteLitigation(litigationId: number): Promise<Response>;
    getAllLitigations(): Promise<ILitigationData[]>;
    addLitigation(number: string, temporaryNumber: string): Promise<Response>;
}

class LitigationService implements ILitigationService {
    async getLitigation(litigationId: number): Promise<ILitigationData> {
        const response = await api.fetch(`${API_URL}${LITIGATION_URL}/${litigationId}`, {
            method: 'GET',
            headers: {
                Authorization: TOKEN_AUTH
            }
        })

        return await response.json();
    }

    async updateLitigation(litigationId: number, newNumber: string, newTemporaryNumber: string): Promise<Response> {
        const dataLitigation: ILitigationUploadData = {
            number: newNumber,
            temporaryNumber: newTemporaryNumber
        }

        const response = await api.fetch(`${API_URL}${LITIGATION_URL}/${litigationId}`, {
            method: 'PUT',
            headers: {
                Authorization: TOKEN_AUTH,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(dataLitigation)
        })

        return response;
    }

    async deleteLitigation(litigationId: number): Promise<Response> {
        const response = await api.fetch(`${API_URL}${LITIGATION_URL}/${litigationId}`, {
            method: 'DELETE',
            headers: {
                Authorization: TOKEN_AUTH,
            },
        });

        return response;
    }

    async getAllLitigations(): Promise<ILitigationData[]> {
        const response = await api.fetch(`${API_URL}${LITIGATION_URL}`, {
            method: 'GET',
            headers: {
                Authorization: TOKEN_AUTH,
            },
        });

        return await response.json();
    }

    async addLitigation(number: string, temporaryNumber: string): Promise<Response> {
        const dataLitigation: ILitigationUploadData = {
            number: number,
            temporaryNumber: temporaryNumber
        }

        const response = await api.fetch(`${API_URL}${LITIGATION_URL}`, {
            method: 'POST',
            headers: {
                Authorization: TOKEN_AUTH,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(dataLitigation)
        })

        return response;
    }
}

const litigationService = new LitigationService();
export default litigationService;