import api from "./api.config";
import { API_URL } from "./api.config";

const AUTH_URL = `:3111/gateway-api-backend/auth`

interface IAuthService {
    login(username: string, password: string): Promise<Response>;
    getTokens(refreshToken: string): Promise<Response>
}

class AuthService implements IAuthService {
    async login(username: string, password: string): Promise<Response> {
        const url = `${API_URL}${AUTH_URL}/get-tokens?grantType=password&username=${username}&password=${password}`

        const response = await api.fetch(url, {
            method: 'GET'
        })

        // const data = await response.json()
        // console.log(data)
        // const acces = data.access_token
        // console.log(`access: ${acces}`)

        return response;
    }

    async getTokens(refreshToken: string): Promise<Response> {
        const url = `${API_URL}${AUTH_URL}/get-tokens?grantType=refreshToken&refreshToken=${refreshToken}`

        const response = await fetch(url, {
            method: 'GET'
        })

        return response;
    }
}

const authService = new AuthService();
export default authService;