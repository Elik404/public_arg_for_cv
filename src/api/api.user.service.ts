import { IUserData } from "../models/users/IUserData";
import { IUserRegisterData } from "../models/users/IUserRegisterData";
import { API_URL, TOKEN_AUTH } from "./api.config";
import api from "./api.config";

const USERS_URL = `:3111/gateway-api-backend/users`

interface IUserService {
    getAllUsers(): Promise<IUserData[]>;
    updateUser(id: string, fio: string, username: string): Promise<Response>;
    registerUser(fio: string, username: string, password: string): Promise<Response>;
    deleteUser(uuid: string): Promise<Response>;
}

class UserService implements IUserService {
    async getAllUsers(): Promise<IUserData[]> {
        const response = await api.fetch(`${API_URL}${USERS_URL}`,
            {
                method: 'GET',
                headers: {
                    Authorization: TOKEN_AUTH,
                },
            })

        return await response.json();
    }

    async updateUser(id: string, fio: string, username: string): Promise<Response> {
        const names = fio.split(' ')
        const firstname = names[1]
        const lastName = names[0]
        const patronymic = names.slice(2).join(' ')

        const dataUser: IUserData = {
            id: id,
            username: username,
            firstName: firstname,
            lastName: lastName,
            patronymic: patronymic,
        }

        const response = await api.fetch(`${API_URL}${USERS_URL}`, {
            method: 'PUT',
            headers: {
                Authorization: TOKEN_AUTH,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(dataUser)
        })

        return response;
    }

    async registerUser(fio: string, username: string, password: string): Promise<Response> {
        const names = fio.split(' ')
        const firstname = names[1]
        const lastName = names[0]
        const patronymic = names.length > 2 ? names.slice(2).join(' ') : ''

        const dataUserRegister: IUserRegisterData = {
            username: username,
            firstName: firstname,
            lastName: lastName,
            patronymic: patronymic,
            password: password,
        }
        const response = await api.fetch(`${API_URL}${USERS_URL}/register`, {
            method: 'POST',
            headers: {
                Authorization: TOKEN_AUTH,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(dataUserRegister),
        })

        return response;
    }

    async deleteUser(uuid: string): Promise<Response> {
        const response = await api.fetch(`${API_URL}${USERS_URL}/${uuid}`,
            {
                method: 'DELETE',
                headers: {
                    Authorization: TOKEN_AUTH,
                },
            })

        return response;
    }
}

const userService = new UserService();
export default userService;