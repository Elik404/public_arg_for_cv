import { IOrderPair } from "../models/photos/IOrderPair";
// import { IPhotoAddData } from "../models/photos/IPhotoAddData";
import { IPhotoData } from "../models/photos/IPhotoData";
// import { IPhotoUpdateData } from "../models/photos/IPhotoUpdateData";
import { API_URL, TOKEN_AUTH } from "./api.config";
import api from "./api.config";

const PHOTOS_URL = `:3333/photo-expertise-backend/api/general-plan-photos`

interface IPhotosService {
    getLitigationPhoto(generalPlanPhotoId: number): Promise<IPhotoData>;
    updatePhoto(generalPlanPhotoId: number, newComment?: string, newPhotoOrder?: number, newPhotoFile?: File): Promise<Response>;
    deletePhoto(generalPlanPhotoId: number): Promise<Response>;
    changePhotoOrders(changedOrders: IOrderPair[]): Promise<Response>;
    uploadPhoto(litigationId: number, comment: string, photoOrder: number, photoFile: File): Promise<Response>;
    getTable(litigationId: number): Promise<Response>;
    getLitigationPhotos(litigationId: number): Promise<IPhotoData[] | undefined>;
}

class PhotosService implements IPhotosService {
    async getLitigationPhoto(generalPlanPhotoId: number): Promise<IPhotoData> {
        const response = await api.fetch(`${API_URL}${PHOTOS_URL}/${generalPlanPhotoId}`, {
            method: 'GET',
            headers: {
                Authorization: TOKEN_AUTH,
            },
        });

        return await response.json();
    }

    async updatePhoto(generalPlanPhotoId: number, newComment?: string, newPhotoOrder?: number, newPhotoFile?: File): Promise<Response> {
        const photoUpdateFormData = new FormData();
        if (newComment)
            photoUpdateFormData.append('newComment', newComment)
        if (newPhotoOrder)
            photoUpdateFormData.append('newPhotoOrder', newPhotoOrder.toString());
        if (newPhotoFile)
            photoUpdateFormData.append('newPhotoFile', newPhotoFile);

        const response = await api.fetch(`${API_URL}${PHOTOS_URL}/${generalPlanPhotoId}`, {
            method: 'PUT',
            headers: {
                Authorization: TOKEN_AUTH,
            },
            body: photoUpdateFormData
        })

        return response;
    }

    async deletePhoto(generalPlanPhotoId: number): Promise<Response> {
        const response = await api.fetch(`${API_URL}${PHOTOS_URL}/${generalPlanPhotoId}`, {
            method: 'DELETE',
            headers: {
                Authorization: TOKEN_AUTH,
            },
        });

        return response;
    }

    async changePhotoOrders(changedOrders: IOrderPair[]): Promise<Response> {
        const response = await api.fetch(`${API_URL}${PHOTOS_URL}/change-photo-orders`, {
            method: 'PUT',
            headers: {
                Authorization: TOKEN_AUTH,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(changedOrders)
        })

        return response;
    }

    async uploadPhoto(litigationId: number, comment: string, photoOrder: number, photoFile: File): Promise<Response> {
        const photoAddFormData = new FormData();
        photoAddFormData.append('litigationId', litigationId.toString());
        photoAddFormData.append('comment', comment);
        photoAddFormData.append('photoOrder', photoOrder.toString());
        photoAddFormData.append('photoFile', photoFile);


        const response = await api.fetch(`${API_URL}${PHOTOS_URL}`, {
            method: 'POST',
            headers: {
                Authorization: TOKEN_AUTH,
            },
            body: photoAddFormData
        })

        return response;
    }

    async getTable(litigationId: number): Promise<Response> {
        const response = await api.fetch(`${API_URL}${PHOTOS_URL}/${litigationId}/word-table`, {
            method: 'GET',
            headers: {
                Authorization: TOKEN_AUTH,
                'Content-Type': 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            },
        });

        return response;
    }

    async getLitigationPhotos(litigationId: number): Promise<IPhotoData[] | undefined> {
        const response = await api.fetch(`${API_URL}${PHOTOS_URL}/${litigationId}/list`, {
            method: 'GET',
            headers: {
                Authorization: TOKEN_AUTH,
            },
        });

        if (response.ok)
            return await response.json();
        return undefined
    }
}

const photosService = new PhotosService();
export default photosService;