export interface IError {
    timestamp: string,
    path: string,
    errorMessage: string,
}