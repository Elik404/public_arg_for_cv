export interface IAuthData {
    refresh_token: string,
    access_token: string
}