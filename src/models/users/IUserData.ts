export interface IUserData {
    id: string;
    username: string;
    firstName: string;
    lastName: string;
    patronymic: string;
}