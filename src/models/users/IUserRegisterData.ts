export interface IUserRegisterData {
    username: string;
    firstName: string;
    lastName: string;
    patronymic: string;
    password: string;
}