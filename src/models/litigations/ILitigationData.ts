export interface ILitigationData {
    id: number,
    number: string,
    temporaryNumber: string;
}