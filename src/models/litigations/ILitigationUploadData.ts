export interface ILitigationUploadData {
    number: string;
    temporaryNumber: string;
}