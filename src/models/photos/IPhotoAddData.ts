export interface IPhotoAddData {
    litigationId: number,
    comment: string,
    photoOrder: number,
    photoFile: File
}