export interface IPhotoUpdateData {
    newComment?: string;
    newPhotoOrder?: number;
    newPhotoFile?: File;
}