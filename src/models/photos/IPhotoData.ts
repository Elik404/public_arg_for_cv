export interface IPhotoData {
    generalPlanPhotoId: number;
    comment: string;
    photoOrder: number;
    contentType: string;
    bytes: [string]
}