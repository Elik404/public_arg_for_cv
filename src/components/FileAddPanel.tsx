import React from 'react';
import PhotoUploader from '../utilities/files/PhotoUploader';
import { useLocation, useNavigate } from 'react-router-dom';

const FileAddPanel: React.FC = () => {
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const litigationId = Number(searchParams.get('litigationId')!);
  const litigationNumber = searchParams.get('litigationNumber');
  const temporaryNumber = searchParams.get('temporaryNumber');
  const maxOrder = searchParams.get('maxOrder')
  const photoOrder = (maxOrder && maxOrder !== '0') ? Number(maxOrder) + 1 : 1
  const navigate = useNavigate()

  interface IUploadParams {
    litigationId: number;
    photoOrder: number;
  }

  const uploadParams: IUploadParams = {
    litigationId: litigationId,
    photoOrder: photoOrder
  }

  const handleCancel = () => {
    navigate(`/folder/photos?litigationId=${litigationId}&litigationNumber=${litigationNumber}&temporaryNumber=${temporaryNumber}`)
  };

  return (
    <div className='file-add-panel'>
      <h1>Добавление файла</h1>
      <PhotoUploader params={uploadParams} onCancel={handleCancel} />
    </div>
  );
};

export default FileAddPanel;