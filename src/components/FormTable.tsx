import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import photosService from '../api/api.photos.service';

const FormTable: React.FC = () => {
    const [isTable, setIsTable] = useState<boolean>(false)
    const [errorMessage, setErrorMessage] = useState<string>('')
    const [isLoading, setIsLoading] = useState<boolean>(false)
    const location = useLocation();
    const searchParams = new URLSearchParams(location.search);
    const litigationId = Number(searchParams.get('litigationId')!);
    const litigationNumber = searchParams.get('litigationNumber');
    const temporaryNumber = searchParams.get('temporaryNumber');
    const navigate = useNavigate()

    const downloadFile = async () => {
        setIsLoading(true)
        try {
            const response = await photosService.getTable(litigationId)
            if (!response.ok) {
                console.log(`HTTP error! Status: ${response.status}`);
            }

            const blob = await response.blob();
            const urlBlob = URL.createObjectURL(blob);
            const aLink = document.createElement('a');
            aLink.href = urlBlob;
            aLink.download = `Таблица.${temporaryNumber}.docx`;
            document.body.appendChild(aLink);
            aLink.click();
            document.body.removeChild(aLink);
            URL.revokeObjectURL(urlBlob);
        }
        catch (error) {
            setErrorMessage('Ошибка формирования таблицы')
        } finally {
            setIsLoading(false);
        }
    }
    useEffect(() => {
        const formTable = async () => {
            setIsLoading(true)
            if (litigationId) {
                try {
                    const response = await photosService.getTable(litigationId)
                    if (response.status < 400) {
                        setIsTable(true)
                    }
                    else {
                        setErrorMessage('Ошибка формирования таблицы')
                    }
                }
                catch (e) {
                    setErrorMessage('Ошибка формирования таблицы')
                    setIsLoading(false)
                } finally {
                    setIsLoading(false);
                }
            }
        };

        formTable();
    }, [litigationId]);


    const handleReturn = () => {
        navigate(`/folder?litigationId=${litigationId}&litigationNumber=${litigationNumber}&temporaryNumber=${temporaryNumber}`)
    }

    return (
        <div className='form-table-panel'>
            <h1>{isLoading ? "Формирование таблицы..." : ""}</h1>
            {isTable ? <div className='table-formed'>
                <h1>Таблица сформирована успешно!</h1>
                <button className="download-btn" onClick={downloadFile}>Скачать</button>
            </div>
                :
                <div className='table-error'>
                    <h1 className='error'>{errorMessage}</h1>
                </div>}
            <button className='home-btn' onClick={handleReturn}>Вернуться назад</button>
        </div>);
};

export default FormTable;