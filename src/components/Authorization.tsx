import React, { useContext, useEffect, useState } from 'react';
import { Context } from '..';
import { observer } from 'mobx-react-lite';
import { useNavigate } from 'react-router-dom';
import cookiesService from '../utilities/storages/CookiesService';

const Authorization: React.FC = () => {
  const [username, setUsername] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [loadingText, setLoadingText] = useState<string>('')
  const { store } = useContext(Context)
  const navigate = useNavigate();

  const handleUsernameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setUsername(event.target.value);
  };

  const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value);
  };

  const handleLogin = async () => {
    setLoadingText('Производится вход...')
    cookiesService.setCookie('isAuth', 'true')
    // try { await store.login(username, password) }
    // catch (e) {
    //   console.log(`failedAuth: ${e}`)
    // }

    // await new Promise(resolve => setTimeout(resolve, 5000));
    // console.log(`cookie: ${document.cookie}`)

    const isAuth = Boolean(cookiesService.getCookie('isAuth'));


    if (isAuth) {
      const route = '/'
      setTimeout(() => {
        setLoadingText('')
        navigate(route);
      }, 3000);
    }
    if (!isAuth) {
      setLoadingText('Ошибка входа')
      console.log(`NOAUTH`)
    }
  };

  useEffect(() => {
    const isAuth = Boolean(cookiesService.getCookie('isAuth'));
    if (isAuth) {
      navigate('/');
    }
  }, [navigate])

  return (
    <div className="sequrity-form" >
      <h1>Вход в <br /> Личный кабинет</h1>
      <input type="text" value={username} onChange={handleUsernameChange} placeholder="Логин"></input>
      <input type="password" value={password} onChange={handlePasswordChange} placeholder="Пароль"></input>
      <button className="submit-btn" onClick={handleLogin}>Войти</button>
      <h3 style={{ color: loadingText === "Ошибка входа" ? "red" : 'black' }}>{loadingText}</h3>
    </div>
  );
}

export default observer(Authorization);
