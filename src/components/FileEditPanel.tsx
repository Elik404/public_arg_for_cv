import React from 'react';
import PhotoEditor from '../utilities/files/PhotoEditor';
import { useLocation, useNavigate } from 'react-router-dom';

const FileEditPanel: React.FC = () => {
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const litigationId = Number(searchParams.get('litigationId')!);
  const litigationNumber = searchParams.get('litigationNumber');
  const temporaryNumber = searchParams.get('temporaryNumber');
  const generalPlanPhotoId = Number(searchParams.get('generalPlanPhotoId')!)
  const comment = searchParams.get('comment');
  const photoOrder = searchParams.get('photoOrder') ? Number(searchParams.get('photoOrder')) : 1
  const navigate = useNavigate()

  interface IUploadParams {
    generalPlanPhotoId: number;
    litigationId: number;
    photoOrder: number;
    comment: string;
  }

  const uploadParams: IUploadParams = {
    generalPlanPhotoId: generalPlanPhotoId,
    litigationId: litigationId,
    photoOrder: photoOrder,
    comment: comment ? comment : '',
  }

  const handleCancel = () => {
    navigate(`/folder/photos?litigationId=${litigationId}&litigationNumber=${litigationNumber}&temporaryNumber=${temporaryNumber}`)
  };

  return (
    <div className='file-edit-panel'>
      <h1><b>Фото {photoOrder}.</b>
      {comment!.split(' ').length > 3 ? `${comment!.split(' ').slice(0,2).join(' ')}...` : comment}
      </h1>
      <PhotoEditor params={uploadParams} onCancel={handleCancel} />
    </div>
  );
};

export default FileEditPanel;