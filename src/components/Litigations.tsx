import search from "../img/search.png"
import add from "../img/add-case.png"
import dir from "../img/dir.png"
import exit from "../img/exit.png"
import control from "../img/control.png"
import React, { useContext, useEffect, useState } from "react"
import { Context } from ".."
import litigationService from "../api/api.litigation.service"
import { ILitigationData } from "../models/litigations/ILitigationData"
import cookiesService from "../utilities/storages/CookiesService"

const Litigations: React.FC = () => {
  const [litigations, setLitigations] = useState<ILitigationData[] | undefined>(undefined);
  const [searchLitigation, setSearchLitigation] = useState<string>('');
  const [nullSearch, setNullSearch] = useState<string>('');

  const { store } = useContext(Context)


  useEffect(() => {
    const fetchLitigations = async () => {
      try {
        // const fetchedLititgations = await litigationService.getAllLitigations();
        const fetchedLititgations = [{id: 1, number: '12', temporaryNumber: '12'} ];
        setLitigations(fetchedLititgations);
      } catch (error) {
        console.error('Error fetching litigations:', error);
      }
    };

    fetchLitigations();
  }, []);

  const handleSearchLitigation = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchLitigation(event.target.value)
    onSearch()
  }

  const filteredLitigations = litigations
    ? litigations.filter((litigationItem) => {
      const temporaryNumber = litigationItem.temporaryNumber;
      const regex = new RegExp(searchLitigation, 'i');
      return regex.test(temporaryNumber);
    })
    : undefined;

  const onSearch = () => {
    setNullSearch(filteredLitigations === undefined || filteredLitigations.length === 0 ? `Не найдено дело с номером: ${searchLitigation}` : '')
  }

  const handleExit = async () => {
    await store.logout()
  }

  const username = cookiesService.getCookie('name')
  const isAdmin = Boolean(cookiesService.getCookie('isAdmin'))

  return (
    <div className="directories">
      <div className="directories-header">
        <div className="left-header">
          {isAdmin ? <h1>ЛК Администратора<br />{username}</h1> : <h1>ЛК Эксперта<br />{username}</h1>}
          <div className="searching">
            <input type="text" placeholder="Введите номер дела" onChange={handleSearchLitigation}></input>
            <button className="search"><img src={search} className="image" onClick={onSearch} alt="Search" /></button>
          </div>
        </div>
        <div className="right-header">
          {isAdmin ?
            <a className="exit-btn" href="/admin"><img src={control} className="image" alt="Home" /></a>
            :
            <a className="exit-btn" href="/login" onClick={handleExit}><img src={exit} className="image" alt="Exit" /></a>}
        </div>
      </div>
      <hr></hr>
      <div className="dirs">
        <a className="case" href="/add-case"><img src={add} className="image" alt="Add" />Новое дело</a>
        <p style={{ color: 'red' }}>{nullSearch}</p>
        {filteredLitigations ? filteredLitigations.map((litigationItem: ILitigationData) => (
          <a key={litigationItem.id} className="case"
            href={`/folder?litigationId=${litigationItem.id}&litigationNumber=${litigationItem.number}&temporaryNumber=${litigationItem.temporaryNumber}`}>
            <img src={dir} className="image" alt="Case" />
            {`${litigationItem.temporaryNumber}`}
          </a>
        )) : <p>Данные о делах не найдены !</p>}
      </div>
    </div>
  );
}

export default Litigations;