import dir from "../img/dir.png"
import add from "../img/add-file.png"
import get from "../img/get.png"
import refresh from "../img/refresh.png"
import { useLocation } from "react-router-dom";
import React, { useEffect, useState } from "react";
import { IPhotoData } from "../models/photos/IPhotoData";
import indexedDBService from "../utilities/storages/IndexedDBService";
import photosService from "../api/api.photos.service";
import { IOrderPair } from "../models/photos/IOrderPair";

const LitigationPhotos: React.FC = () => {
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const litigationId = Number(searchParams.get('litigationId')!);
  const litigationNumber = searchParams.get('litigationNumber');
  const temporaryNumber = searchParams.get('temporaryNumber');
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [photos, setPhotos] = useState<IPhotoData[] | undefined>(undefined);

  const [isMoving, setIsMoving] = useState<boolean>(false)
  const [isOrdering, setIsOrdering] = useState<boolean>(false)

  const refreshPhotos = async () => {
    setPhotos(undefined)
    setIsLoading(true)
    try {
      const fetchedPhotos = await photosService.getLitigationPhotos(litigationId);
      if (fetchedPhotos) {
        indexedDBService.savePhotos(litigationId, fetchedPhotos)
        setPhotos(fetchedPhotos)
      }
    }
    catch (error) {
      console.error('Error fetching photos:', error);
    } finally {
      setIsLoading(false);
    }
  }

  const handleDragStart = (e: React.DragEvent<HTMLAnchorElement>, photoOrder: number) => {
    e.dataTransfer.setData("text/plain", photoOrder.toString());
  };

  const handleDragOver = (e: React.DragEvent<HTMLAnchorElement>) => {
    e.preventDefault();
  };

  const handleContextMenu = (e: React.MouseEvent<HTMLAnchorElement>, curPhotoOrder: number) => {
    e.preventDefault();
    if (!isMoving) {
      setSelectedOrder(curPhotoOrder)
      setCurrentOrder(curPhotoOrder)
      setIsOrdering(true)
    }
  };


  const handleDrop = async (e: React.DragEvent<HTMLAnchorElement>, targetPhotoOrder: number) => {
    e.preventDefault();
    if (!isMoving) {
      setIsMoving(true)
      console.log('start moving')
      try {
        const draggedPhotoOrder = parseInt(e.dataTransfer.getData("text/plain"));
        console.log(`d: ${draggedPhotoOrder}, t: ${targetPhotoOrder}`)

        if (draggedPhotoOrder !== targetPhotoOrder) {
          const photoOrders = photos ? photos.map((photo) => photo.photoOrder) : [0];
          const updatedPhotoOrders = [...photoOrders];
          const draggedIndex = updatedPhotoOrders.indexOf(draggedPhotoOrder);
          updatedPhotoOrders.splice(draggedIndex, 1);
          updatedPhotoOrders.splice(targetPhotoOrder - 1, 0, draggedPhotoOrder);

          setPhotos((prevPhotos) => {
            const updatedPhotos = prevPhotos
              ?.sort((a, b) => updatedPhotoOrders.indexOf(a.photoOrder) - updatedPhotoOrders.indexOf(b.photoOrder))
              .map((photo, index) => ({
                ...photo,
                photoOrder: index + 1,
              }));
            return updatedPhotos;
          })

          const orderChanges: IOrderPair[] | undefined = photos?.map((photo) => ({
            generalPlanPhotoId: photo.generalPlanPhotoId,
            newPhotoOrder: updatedPhotoOrders.indexOf(photo.photoOrder) + 1,
          }));

          if (orderChanges) {
            try {
              await photosService.changePhotoOrders(orderChanges);
              if (photos) {
                indexedDBService.savePhotos(litigationId, photos);
              }
            } catch (e) {
              console.error(`Error changing orders: ${e}`);
            }
          };
        }
      }
      catch (e) { console.error(`Erro changing: ${e}`) }
      finally {
        setIsMoving(false)
        console.log('moved')
      }
    }
  };

  useEffect(() => {
    const fetchPhotos = async () => {
      setIsLoading(true)
      try {
        let data = await indexedDBService.getPhotos(litigationId);
        if (data) {
          console.log('Photos exist in IndexedDB')
          setPhotos(data)
        }
        if (!data) {
          console.log('Photos dont exist in IndexedDB')
          const fetchedPhotos = await photosService.getLitigationPhotos(litigationId);
          if (fetchedPhotos && fetchedPhotos.length !== 0) {
            indexedDBService.savePhotos(litigationId, fetchedPhotos)
            setPhotos(fetchedPhotos)
          }
        }
      } catch (error) {
        console.error('Error fetching photos:', error);
      } finally {
        setIsLoading(false);
      }
    };

    fetchPhotos();
  }, [litigationId]);

  const [selectedOrder, setSelectedOrder] = useState<number>(1);
  const [currentOrder, setCurrentOrder] = useState<number>(1);

  const handleOrderChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setSelectedOrder(Number(e.target.value));
  };

  const handleOrdering = async () => {
    setIsOrdering(false)
    console.log(`d: ${currentOrder}, t: ${selectedOrder}`)
    if (currentOrder !== selectedOrder) {
      setIsMoving(true)
      console.log('start moving')
      try {
        const photoOrders = photos ? photos.map((photo) => photo.photoOrder) : [0];
        const updatedPhotoOrders = [...photoOrders];
        const draggedIndex = updatedPhotoOrders.indexOf(currentOrder);
        updatedPhotoOrders.splice(draggedIndex, 1);
        updatedPhotoOrders.splice(selectedOrder - 1, 0, currentOrder);

        setPhotos((prevPhotos) => {
          const updatedPhotos = prevPhotos
            ?.sort((a, b) => updatedPhotoOrders.indexOf(a.photoOrder) - updatedPhotoOrders.indexOf(b.photoOrder))
            .map((photo, index) => ({
              ...photo,
              photoOrder: index + 1,
            }));
          return updatedPhotos;
        })

        const orderChanges: IOrderPair[] | undefined = photos?.map((photo) => ({
          generalPlanPhotoId: photo.generalPlanPhotoId,
          newPhotoOrder: updatedPhotoOrders.indexOf(photo.photoOrder) + 1,
        }));

        if (orderChanges) {
          try {
            await photosService.changePhotoOrders(orderChanges);
            if (photos) {
              indexedDBService.savePhotos(litigationId, photos);
            }
          } catch (e) {
            console.error(`Error changing orders: ${e}`);
          }
        };

      }
      catch (e) { console.error(`Error changing: ${e}`) }
      finally {
        setIsMoving(false)
        console.log('moved')
      }

    }
  }

  const photoOrders = photos && photos.length !== 0 ? photos.map(photo => photo.photoOrder) : [0];
  const maxPhotoOrder = Math.max(...photoOrders);

  return (
    <div className="folder-photos">
      <div className="photos-header">
        <div className="left-header">
          <a className="case" href={`/folder?litigationId=${litigationId}&litigationNumber=${litigationNumber}&temporaryNumber=${temporaryNumber}`}>
            <img src={dir} className="image" alt="Dir" />
          </a>
          <h1>{temporaryNumber}<br />Общие планы</h1>
        </div>
        <button className="refresh-btn" onClick={refreshPhotos}><img src={refresh} className="image" alt="Обновить" /></button>
      </div>
      <hr></hr>

      <div className="images">
        <h1>{isLoading ? "Загрузка фотографий..." : ""}</h1>
        {photos && photos.length > 0 ? photos.map((photo: IPhotoData) => (
          <a href={`./photos/edit?litigationId=${litigationId}&litigationNumber=${litigationNumber}&temporaryNumber=${temporaryNumber}&photoOrder=${photo.photoOrder}&comment=${photo.comment}&generalPlanPhotoId=${photo.generalPlanPhotoId}`}
            key={`${photo.generalPlanPhotoId}`} className="photo-item"
            draggable
            onDragStart={(e) => handleDragStart(e, photo.photoOrder)}
            onDragOver={(e) => handleDragOver(e)}
            onContextMenu={(e) => handleContextMenu(e, photo.photoOrder)}
            onDrop={(e) => handleDrop(e, photo.photoOrder)}>
            <img src={`data:${photo.contentType};base64,${photo.bytes}`} alt={photo.comment} draggable={false} />
            <p><b>{`Фото ${photo.photoOrder}.`}</b> {`${photo.comment}`}</p>  </a>
        )) : <h2>В деле пока нет фотографий</h2>}
      </div>
      <hr></hr>
      <div className="folder-footer">
        <a className="add-file" href={`./photos/add?litigationId=${litigationId}&litigationNumber=${litigationNumber}&temporaryNumber=${temporaryNumber}&maxOrder=${maxPhotoOrder < 0 ? 0 : maxPhotoOrder}`}>
          <img src={add} className="image" alt="Add" />Добавить файл</a>
        <a className="form-table" href={`./form-table?litigationId=${litigationId}&litigationNumber=${litigationNumber}&temporaryNumber=${temporaryNumber}`}><img src={get} className="image" alt="Table" />Сформировать<br />таблицу</a>
      </div>

      {isOrdering && (
        <div className="order-modal">
          <p>Выберите новый номер:</p>
          <select value={selectedOrder} onChange={handleOrderChange}>
            {Array.from({ length: maxPhotoOrder }, (_, index) => index + 1).map((order) => (
              <option key={order} value={order}>
                {order}
              </option>
            ))}
          </select>
          <button onClick={handleOrdering}>Ок</button>
        </div>
      )}
    </div>
  );
}

export default LitigationPhotos;