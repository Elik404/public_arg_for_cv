import React, { useEffect, useState } from 'react';
import home from "../img/home.png"
import { useLocation, useNavigate } from 'react-router-dom';
import userService from '../api/api.user.service';


const EditUser: React.FC = () => {
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const fullNameOld = searchParams.get('fullname');
  const userNameOld = searchParams.get('username');
  const codedId = searchParams.get('id')
  const id = codedId ? atob(codedId) : '';

  const [fio, setFio] = useState<string>('');
  const [login, setLogin] = useState<string>('');
  const [errorLoginText, setErrorLoginText] = useState<string>('');
  const [errorValidationText, setErrorValidationText] = useState<string>('');
  const [isDeleteModalVisible, setDeleteModalVisible] = useState<boolean>(false);

  const [countDown, setCountDown] = useState<string>('')
  const [color, setColor] = useState<string>('')
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [loadingResultMessage, setLoadingResultMessage] = useState<string>('')
  const navigate = useNavigate()

  const handleFioChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const inputText = event.target.value;

    const regex = /^(?:\D+\s+){2}\D+$/;
    const hasError = !regex.test(inputText);

    setErrorLoginText(hasError ? '! ФИО должно состоять минимум из трех слов и не содержать цифр !' : '');
    setFio(inputText);
  };

  const handleLoginChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setLogin(event.target.value);
  };

  const handleUpdate = async () => {
    if (Validation()) {
      setIsLoading(true)
      setColor('black')
      setLoadingResultMessage('Обновление...')

      try {
        const response = await userService.updateUser(id, fio, login)
        if (response.status < 400) {
          setColor('green')
          setLoadingResultMessage(`Пользователь ${fio} успешно обновлен!`)
        }
        else {
          setColor('red')
          setLoadingResultMessage('Ошибка обновления пользователя')
        }
      } catch (error) {
        setColor('red')
        setLoadingResultMessage('Ошибка обновления!')
      } finally {
        setCountDown('Переход через 3...')
        setTimeout(() => {
          setCountDown('Переход через 2...')
        }, 1000)
        setTimeout(() => {
          setCountDown('Переход через 1...')
        }, 2000)

        setTimeout(() => {
          setIsLoading(false)
        }, 3000)
        setTimeout(() => {
          navigate(`/admin/users`)
        }, 3500);
      }
    }
  };

  const Validation = (): boolean => {
    const validationResult = !!fio && !!login && !errorLoginText
    setErrorValidationText(!validationResult ? '! Все поля должны быть верно заполнены !' : '')
    return validationResult
  }

  const toggleDeleteModal = () => {
    setDeleteModalVisible(!isDeleteModalVisible);
  };

  const handleDelete = async () => {
    toggleDeleteModal();
  }

  const confirmDelete = async () => {
    toggleDeleteModal();
    setIsLoading(true)
    setColor('black')
    setLoadingResultMessage('Удаление...')

    try {
      const response = await userService.deleteUser(id);
      if (response.status < 400) {
        setColor('green')
        setLoadingResultMessage(`Пользователь ${fullNameOld} успешно удален!`);
      }
      else {
        setColor('red')
        setLoadingResultMessage('Ошибка удаления!')
      }
    } catch (error) {
      setColor('red')
      setLoadingResultMessage('Ошибка удаления!')
      console.log(`Error deleting: ${error}`)
    } finally {
      setCountDown('Переход через 3...')
      setTimeout(() => {
        setCountDown('Переход через 2...')
      }, 1000)
      setTimeout(() => {
        setCountDown('Переход через 1...')
      }, 2000)

      setTimeout(() => {
        setIsLoading(false)
      }, 3000)
      setTimeout(() => {
        navigate('/admin/users');
      }, 3500);
    }

  };

  useEffect(() => {
    const fillData = () => {
      setFio(fullNameOld!)
      setLogin(userNameOld!)
    };

    fillData();

  }, [fullNameOld, userNameOld]);

  return (
    <div className="edit-user-panel">
      <div className="edit-user-header">
        <h1> Панель <br /> Администратора </h1>
        <a className="home-btn" href="/admin/users"><img src={home} className="image" alt="Home" /></a>
      </div>
      <hr></hr>
      <div className="edit-user-form">
        <h1>Редактирование<br /> пользователя</h1>
        <p>Введите новое ФИО</p>
        <input type="text" placeholder={fullNameOld!} value={fio} onChange={handleFioChange}></input>
        <div style={{ color: 'red' }}>{errorLoginText}</div>
        <p>Введите новый логин</p>
        <input type="text" placeholder={userNameOld!} value={login} onChange={handleLoginChange}></input>
        <button className="submit-btn" onClick={handleUpdate}>Обновить пользователя</button>
        <div style={{ color: 'red' }}>{errorValidationText}</div>
        <button className="delete-btn" onClick={handleDelete}>Удалить пользователя</button>
      </div>

      {isDeleteModalVisible && (
        <div className="delete-modal">
          <p>Вы уверены, что хотите <b>удалить</b> пользователя?</p>
          <button className='yes' onClick={confirmDelete}>Да</button>
          <button className='no' onClick={toggleDeleteModal}>Нет</button>
        </div>
      )}

      {isLoading && (
        <div className="load-modal">
          <p style={{ color: color }}>{loadingResultMessage}</p>
          <p style={{ color: 'grey', fontSize: '16px' }}>{countDown}</p>
        </div>
      )}
    </div>
  );
}

export default EditUser;