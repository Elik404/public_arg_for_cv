import dir from "../img/dir.png"
import user from "../img/user.png"
import exit from "../img/exit.png"
import { useContext } from "react"
import { Context } from ".."
import { useNavigate } from "react-router-dom"


function AdministratorPanel() {
  const { store } = useContext(Context)
  const navigate = useNavigate();

  const handleExit = async () => {
    await store.logout()
    navigate('/login');
  }
 
  return (
    <div className="admin-panel">
      <div className="admin-header">
        <h1> Панель <br /> Администратора </h1>
        <button className="exit-btn" onClick={handleExit}><img src={exit} className="image" alt="exit" /></button>
      </div>
      <hr></hr>

      <div className="buttons">
        <a className="add-user" href="admin/add-user"><img src={user} className="image" alt="UserAdd" />Добавить<br />пользователя</a>
        <a className="all-users" href="admin/users"><img src={user} className="image" alt="Users" />Список<br />пользователей</a>
        <a className="open-cases" href="./"><img src={dir} className="image" alt="Cases" />Открыть дела</a>
      </div>
    </div>
  );
}

export default AdministratorPanel;