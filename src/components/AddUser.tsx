import React, { useState } from 'react';
import home from "../img/home.png"
import userService from '../api/api.user.service';
import { useNavigate } from 'react-router-dom';


const AddUser: React.FC = () => {
  const [fio, setFio] = useState<string>('');
  const [login, setLogin] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [errorLoginText, setErrorLoginText] = useState<string>('');
  const [errorPasswordText, setErrorPasswordText] = useState<string>('');
  const [errorValidationText, setErrorValidationText] = useState<string>('');
  const [isLoginChanged, setIsLoginChanged] = useState<boolean>(false)

  const [countDown, setCountDown] = useState<string>('')
  const [color, setColor] = useState<string>('')
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [loadingResultMessage, setLoadingResultMessage] = useState<string>('')
  const navigate = useNavigate();

  var isHttpError: boolean = false
  const [isDetailingButton, setIsDetailingButton] = useState<boolean>(false)
  const [isDetailing, setIsDetailing] = useState<boolean>(false)

  const handleFioChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const inputText = event.target.value;

    const regex = /^(?:\D+\s+){2}\D+$/;
    const hasError = !regex.test(inputText);

    setErrorLoginText(hasError ? '! ФИО должно состоять минимум из трех слов и не содержать цифр !' : '');
    setFio(inputText);

    if (!isLoginChanged) {
      const words = inputText.split(' ');
      const translit = words.slice(0, 2).map(word => transliterate(word)).join('.');
      setLogin(translit);
    }
  };

  const handleLoginChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setIsLoginChanged(true)
    setLogin(event.target.value);
  };

  const handlePasswordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const inputText = event.target.value;

    const regex = /^.{8,64}$/;
    const hasError = !regex.test(inputText);

    setErrorPasswordText(hasError ? '! Пароль должен содержать от 8 до 64 символов !' : '');
    setPassword(inputText);
  };

  const transliterate = (text: string): string => {
    const russianChars = {
      а: 'a', б: 'b', в: 'v', г: 'g', д: 'd', е: 'e', ё: 'e', ж: 'zh',
      з: 'z', и: 'i', й: 'i', к: 'k', л: 'l', м: 'm', н: 'n', о: 'o',
      п: 'p', р: 'r', с: 's', т: 't', у: 'u', ф: 'f', х: 'kh', ц: 'ts',
      ч: 'ch', ш: 'sh', щ: 'sch', ъ: '', ы: 'y', ь: 'i', э: 'e', ю: 'yu',
      я: 'ya', А: 'a', Б: 'b', В: 'v', Г: 'g', Д: 'd', Е: 'e', Ё: 'e', Ж: 'zh',
      З: 'z', И: 'i', Й: 'i', К: 'k', Л: 'l', М: 'm', Н: 'n', О: 'o',
      П: 'p', Р: 'r', С: 's', Т: 't', У: 'u', Ф: 'f', Х: 'kh', Ц: 'ts',
      Ч: 'ch', Ш: 'sh', Щ: 'sch', Ъ: '', Ы: 'y', Ь: 'i', Э: 'e', Ю: 'yu', Я: 'ya'
    };

    return text
      .split('')
      .map(char => (russianChars[char as keyof typeof russianChars] ? russianChars[char as keyof typeof russianChars] : char))
      .join('');
  };

  const Validation = (): boolean => {
    const validationResult = !!fio && !!login && !!password && !errorLoginText && !errorPasswordText
    setErrorValidationText(!validationResult ? '! Все поля должны быть верно заполнены !' : '')
    return validationResult
  }

  // const Detailing = () => {
  //   setIsDetailing(true)
  // }

  const handleAdd = async () => {
    if (Validation()) {
      setIsLoading(true)
      setColor('black')
      setLoadingResultMessage('Добавление...')

      try {
        const response = await userService.registerUser(fio, login, password)

        if (response.status < 400) {
          setColor('green')
          setLoadingResultMessage(`Пользователь ${fio} успешно добавлен!`);
        }
        else {
          isHttpError = true
          setIsDetailingButton(true)
          setColor('red')
          setLoadingResultMessage('Ошибка добавления пользователя!')
        }
      } catch (e) {
        setColor('red')
        setLoadingResultMessage('Ошибка добавления!')
      } finally {
        if (!isHttpError) {
          setCountDown('Переход через 5...')
          setTimeout(() => {
            setCountDown('Переход через 4...')
          }, 1000)
          setTimeout(() => {
            setCountDown('Переход через 3...')
          }, 2000)
          setTimeout(() => {
            setCountDown('Переход через 2...')
          }, 3000)
          setTimeout(() => {
            setCountDown('Переход через 1...')
          }, 4000)
          setTimeout(() => {
            setIsLoading(false)
          }, 5000)
          setTimeout(() => {
            navigate('/admin')
          }, 5500)
        }
      }
    }
  };

  return (
    <div className="user-add-panel">
      <div className="user-add-header">
        <h1> Панель <br /> Администратора </h1>
        <a className="home-btn" href="/admin"><img src={home} className="image" alt="Home" /></a>
      </div>
      <hr></hr>
      <div className="add-new-user-form">
        <h1>Добавление нового<br /> пользователя</h1>
        <input type="text" placeholder="ФИО" value={fio} onChange={handleFioChange}></input>
        <div style={{ color: 'red' }}>{errorLoginText}</div>
        <input type="text" placeholder="Логин" value={login} onChange={handleLoginChange}></input>
        <input type="text" placeholder="Пароль" value={password} onChange={handlePasswordChange}></input>
        <div style={{ color: 'red' }}>{errorPasswordText}</div>
        <button className="submit-btn" onClick={handleAdd}>Добавить пользователя</button>
        <div style={{ color: 'red' }}>{errorValidationText}</div>
      </div>

      {isLoading && (
        <div className="load-modal">
          <button className="cancel" onClick={() => { navigate('/admin') }}>Х</button>
          <p style={{ color: color }}>{loadingResultMessage}</p>
          {!isDetailingButton && <p style={{ color: 'grey', fontSize: '16px' }}>{countDown}</p>}
          {isDetailingButton && (<button onClick={() => { setIsDetailing(!isDetailing) }}>Детальнее</button>)}
          {isDetailing && (
            <div className='detailing'>
              <p>timestamp: "{}",</p>
              <p>path: "{}"</p>
              <p>errorMessage: "{}"</p>
            </div>)}
        </div>
      )}
    </div>
  );
}

export default AddUser;