import dir from "../img/dir.png"
import home from "../img/home.png"
import edit from "../img/edit.png"
import schema from "../img/schema.png"
import get from "../img/get.png"
import { useLocation } from "react-router-dom";
import React from "react"

const Litigation: React.FC = () => {
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const litigationId = searchParams.get('litigationId');
  const litigationNumber = searchParams.get('litigationNumber');
  const temporaryNumber = searchParams.get('temporaryNumber');

  return (
    <div className="folder">
      <div className="folder-header">
        <div className="left-header">
          <a className="case" href="/"><img src={dir} className="image" alt="Дело" /></a>
          <h1>{temporaryNumber}</h1>
        </div>
        <div className="settings">
          <a className="edit-btn"
            href={`folder/edit?litigationId=${litigationId}&litigationNumber=${litigationNumber}&temporaryNumber=${temporaryNumber}`}>
            <img src={edit} className="image" alt="Редактировать дело" /></a>
          <a className="home-btn" href="/"><img src={home} className="image" alt="Домой" /></a>
        </div>
      </div>
      <hr></hr>
      <div className="buttons">
        <a className="photos" href={`/folder/photos?litigationId=${litigationId}&litigationNumber=${litigationNumber}&temporaryNumber=${temporaryNumber}`}>
          <img src={dir} className="image" alt="Фото" />Общие планы</a>
        <a className="schema" href="/schema"><img src={schema} className="image" alt="Схема" />Схема</a>
      </div>
      <hr></hr>
      <div className="folder-footer">
        <a className="form-table"
          href={`folder/form-table?litigationId=${litigationId}&litigationNumber=${litigationNumber}&temporaryNumber=${temporaryNumber}`}>
          <img src={get} className="image" alt="Сформировать" />Сформировать<br></br>таблицу фото</a>
      </div>
    </div>
  );
}

export default Litigation;