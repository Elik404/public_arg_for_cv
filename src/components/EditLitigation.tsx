import React, { useEffect, useRef, useState } from 'react';
import { useLocation, useNavigate } from "react-router-dom"
import litigationService from '../api/api.litigation.service';

const EditLitigation: React.FC = () => {
    const [errorMessage, setErrorMessage] = useState<string>('');
    const [color, setColor] = useState<string>('red')
    const [isDeleteModalVisible, setDeleteModalVisible] = useState(false);
    const navigate = useNavigate();

    const [isLoading, setIsLoading] = useState<boolean>(false)
    const [loadingResultMessage, setLoadingResultMessage] = useState<string>('')
    const [countDown, setCountDown] = useState<string>('')

    const location = useLocation();
    const searchParams = new URLSearchParams(location.search);
    const litigationId = Number(searchParams.get('litigationId')!);
    var litigationNumber = searchParams.get('litigationNumber');
    var litigationTemporaryNumber = searchParams.get('temporaryNumber');

    const [firstNumberPart, setFirstNumberPart] = useState<string>('')
    const [secondNumberPart, setSecondNumberPart] = useState<string>('')
    const [thirdNumberPart, setThirdNumberPart] = useState<string>('')
    const [fourthNumberPart, setFourthNumberPart] = useState<string>('')
    const [fifthNumberPart, setFifthNumberPart] = useState<string>('')

    const [firstTemporaryNumberPart, setFirstTemporaryNumberPart] = useState<string>('')
    const [secondTemporaryNumberPart, setSecondTemporaryNumberPart] = useState<string>('')
    const [thirdTemporaryNumberPart, setThirdTemporaryNumberPart] = useState<string>('')

    const firstNumberRef = useRef<HTMLInputElement>(null);
    const secondNumberRef = useRef<HTMLInputElement>(null);
    const thirdNumberRef = useRef<HTMLInputElement>(null);
    const fourthNumberRef = useRef<HTMLInputElement>(null);
    const fifthNumberRef = useRef<HTMLInputElement>(null);

    const firstTemporaryNumberRef = useRef<HTMLInputElement>(null);
    const secondTemporaryNumberRef = useRef<HTMLInputElement>(null);
    const thirdTemporaryNumberRef = useRef<HTMLInputElement>(null);


    const handleStringPart = (
        event: React.ChangeEvent<HTMLInputElement>,
        nextRef: React.RefObject<HTMLInputElement>) => {
        setErrorMessage('')
        const inputText = event.target.value
        const length = event.target.placeholder.length
        if (inputText.length < 3) {
            const regex = new RegExp(`^[0-9]{0,2}$`);
            if (regex.test(inputText)) {
                setFirstNumberPart(inputText)
            }
        }
        else if (inputText.length < 5) {
            const regex = new RegExp(`^[0-9]{0,2}[a-zA-Z]{0,2}$`);
            if (regex.test(inputText)) {
                setFirstNumberPart(inputText.toUpperCase())
            }
        }
        else if (inputText.length < 9) {
            const regex = new RegExp(`^[0-9]{0,2}[a-zA-Z]{0,2}[0-9]{0,4}$`);
            if (regex.test(inputText)) {
                setFirstNumberPart(inputText)
            }
        }
        if (inputText.length === length) {
            if (nextRef.current) {
                nextRef.current.focus()
            }
        }

    }

    const handleNumberPart = (
        event: React.ChangeEvent<HTMLInputElement>,
        nextRef: React.RefObject<HTMLInputElement>) => {
        setErrorMessage('')
        const inputText = event.target.value
        const id = Number(event.target.id)
        const length = event.target.placeholder.length
        const regex = new RegExp(`^[0-9]{0,${length}}$`);
        if (regex.test(inputText)) {
            switch (id) {
                case 2:
                    setSecondNumberPart(inputText)
                    break;
                case 3:
                    setThirdNumberPart(inputText)
                    break;
                case 4:
                    setFourthNumberPart(inputText)
                    break;
                case 5:
                    setFifthNumberPart(inputText)
                    break;
                case 6:
                    setFirstTemporaryNumberPart(inputText)
                    break;
                case 7:
                    setSecondTemporaryNumberPart(inputText)
                    break;
                case 8:
                    setThirdTemporaryNumberPart(inputText)
                    break;
            }

            if (inputText.length === length) {
                if (nextRef.current) {
                    nextRef.current.focus()
                }
            }
        }
        else {
            event.preventDefault()
        }
    }

    const handleBackspace = (
        event: React.KeyboardEvent<HTMLInputElement>,
        prevRef: React.RefObject<HTMLInputElement>
    ) => {
        setErrorMessage('')
        if (event.key === 'Backspace' && event.currentTarget.value === '') {
            event.preventDefault();
            if (prevRef.current) {
                prevRef.current.focus();
            }
        }
    };

    const handleUpdate = async () => {
        const number = `${firstNumberPart}-${secondNumberPart}-${thirdNumberPart}-${fourthNumberPart}-${fifthNumberPart}`
        const temporaryNumber = `${firstTemporaryNumberPart}-${secondTemporaryNumberPart}/${thirdTemporaryNumberPart}`
        const regexNumber = /^([0-9]{2}[A-Z]{2}[0-9]{4}-[0-9]{2}-[0-9]{4}-[0-9]{6}-[0-9]{2})$/;
        const regexTemporaryNumber = /^([0-9]{2}-[0-9]{4}\/[0-9]{4})$/;

        if (!regexNumber.test(number) || !regexTemporaryNumber.test(temporaryNumber)) {
            setErrorMessage('Все поля должны быть заполнены верно!')
        }
        else {
            setIsLoading(true)
            setColor('black')
            setLoadingResultMessage('Обновление...')

            try {
                const response = await litigationService.updateLitigation(litigationId, number, temporaryNumber)
                const error = response.status >= 400
                setColor(error ? 'red' : 'lightgreen')
                setLoadingResultMessage(error ? `Ошибка обновления ${litigationTemporaryNumber}` : `Дело ${temporaryNumber} обновлено успешно!`)
                if (!error) {
                    litigationNumber = number
                    litigationTemporaryNumber = temporaryNumber
                }
            } catch (error) {
                setColor('red')
                if (`${error}`.includes('409'))
                    setLoadingResultMessage(`Ошибка! Дело с таким ИД уже существует!`)
                else
                    setLoadingResultMessage('Ошибка обновления!')
                console.log(`Error updating: ${error}`)
            } finally {
                setCountDown('Переход через 3...')
                setTimeout(() => {
                    setCountDown('Переход через 2...')
                }, 1000)
                setTimeout(() => {
                    setCountDown('Переход через 1...')
                }, 2000)

                setTimeout(() => {
                    setIsLoading(false)
                }, 3000)
                setTimeout(() => {
                    navigate(`/folder?litigationId=${litigationId}&litigationNumber=${litigationNumber}&temporaryNumber=${litigationTemporaryNumber}`)
                }, 3500);
            }
        }
    }

    const handleCancel = () => {
        navigate(`/folder?litigationId=${litigationId}&litigationNumber=${litigationNumber}&temporaryNumber=${litigationTemporaryNumber}`);
    };

    const toggleDeleteModal = () => {
        setDeleteModalVisible(!isDeleteModalVisible);
    };

    const handleDelete = async () => {
        toggleDeleteModal();
    }

    const confirmDelete = async () => {
        toggleDeleteModal();
        setIsLoading(true)
        setColor('black')
        setLoadingResultMessage('Удаление...')

        try {
            const response = await litigationService.deleteLitigation(litigationId)
            if (response && response.status < 400) {
                setColor('green')
                setLoadingResultMessage('Дело удалено успешно')
            }
            else {
                setColor('red')
                setLoadingResultMessage('Ошибка удаления дела')
            }
        } catch (error) {
            setColor('red')
            setLoadingResultMessage('Ошибка удаления дела')
            console.log(`Error deleting: ${error}`)
        } finally {
            setCountDown('Переход через 3...')
            setTimeout(() => {
                setCountDown('Переход через 2...')
            }, 1000)
            setTimeout(() => {
                setCountDown('Переход через 1...')
            }, 2000)


            setTimeout(() => {
                setIsLoading(false)
            }, 3000)
            setTimeout(() => {
                navigate('/')
            }, 3500);
        }
    }

    useEffect(() => {
        const fillData = () => {
            const litigationNumberParts = litigationNumber!.split('-')
            setFirstNumberPart(litigationNumberParts[0])
            setSecondNumberPart(litigationNumberParts[1])
            setThirdNumberPart(litigationNumberParts[2])
            setFourthNumberPart(litigationNumberParts[3])
            setFifthNumberPart(litigationNumberParts[4])

            const litigationTemporaryNumberParts = litigationTemporaryNumber!.split('-')
            setFirstTemporaryNumberPart(litigationTemporaryNumberParts[0])
            setSecondTemporaryNumberPart(litigationTemporaryNumberParts[1].split('/')[0])
            setThirdTemporaryNumberPart(litigationTemporaryNumberParts[1].split('/')[1])
        };

        fillData();

    }, [litigationNumber, litigationTemporaryNumber]);


    return (
        <div className='case-edit-panel'>
            <h1>Редактирование дела<br></br>{litigationTemporaryNumber}</h1>
            <div className='edit-case'>
                <h2>Введите уникальный ИД дела</h2>
                <div className='id-input'>
                    <input type="text" id='1' placeholder="00AA0000"
                        ref={firstNumberRef} value={firstNumberPart}
                        onChange={(e) => handleStringPart(e, secondNumberRef)}
                    ></input>
                    <p>-</p>
                    <input type="text" id='2' placeholder="00"
                        ref={secondNumberRef} value={secondNumberPart}
                        onChange={(e) => handleNumberPart(e, thirdNumberRef)}
                        onKeyDown={(e) => handleBackspace(e, firstNumberRef)}
                    ></input>
                    <p>-</p>
                    <input type="text" id='3' placeholder="0000"
                        ref={thirdNumberRef} value={thirdNumberPart}
                        onChange={(e) => handleNumberPart(e, fourthNumberRef)}
                        onKeyDown={(e) => handleBackspace(e, secondNumberRef)}></input>
                    <p>-</p>
                    <input type="text" id='4' placeholder="000000"
                        ref={fourthNumberRef} value={fourthNumberPart}
                        onChange={(e) => handleNumberPart(e, fifthNumberRef)}
                        onKeyDown={(e) => handleBackspace(e, thirdNumberRef)}></input>
                    <p>-</p>
                    <input type="text" id='5' placeholder="00"
                        ref={fifthNumberRef} value={fifthNumberPart}
                        onChange={(e) => handleNumberPart(e, fifthNumberRef)}
                        onKeyDown={(e) => handleBackspace(e, fourthNumberRef)}></input>
                </div>
                <h3><em>Текущий ID: {litigationNumber}</em></h3>
                <h2>Введите номер дела</h2>
                <div className='number-input'>
                    <input type="text" id='6' placeholder="00"
                        ref={firstTemporaryNumberRef} value={firstTemporaryNumberPart}
                        onChange={(e) => handleNumberPart(e, secondTemporaryNumberRef)}></input>
                    <p>-</p>
                    <input type="text" id='7' placeholder="0000"
                        ref={secondTemporaryNumberRef} value={secondTemporaryNumberPart}
                        onChange={(e) => handleNumberPart(e, thirdTemporaryNumberRef)}
                        onKeyDown={(e) => handleBackspace(e, firstTemporaryNumberRef)}></input>
                    <p><b>/</b></p>
                    <input type="text" id='8' placeholder="0000"
                        ref={thirdTemporaryNumberRef} value={thirdTemporaryNumberPart}
                        onChange={(e) => handleNumberPart(e, thirdTemporaryNumberRef)}
                        onKeyDown={(e) => handleBackspace(e, secondTemporaryNumberRef)}></input>
                </div>
                <h3><em>Текущий номер: {litigationTemporaryNumber}</em></h3>
                <h2 style={{ color: 'red' }}>{errorMessage}</h2>
                <button className="submit-btn" type="button" onClick={handleUpdate}>Обновить дело</button>
                <button className="delete-btn" type="button" onClick={handleDelete}>Удалить дело</button>
                <button className="reset-btn" type="button" onClick={handleCancel}>Отмена</button>

                {isDeleteModalVisible && (
                    <div className="delete-modal">
                        <p>Вы уверены, что хотите удалить <br></br>дело <b>{litigationTemporaryNumber}</b>?</p>
                        <button className='yes' onClick={confirmDelete}>Да</button>
                        <button className='no' onClick={toggleDeleteModal}>Нет</button>
                    </div>
                )}
            </div>

            {isLoading && (
                <div className="load-modal">
                    <p style={{ color: color }}>{loadingResultMessage}</p>
                    <p style={{ color: 'grey', fontSize: '16px' }}>{countDown}</p>
                </div>
            )}
        </div>
    );
};

export default EditLitigation;