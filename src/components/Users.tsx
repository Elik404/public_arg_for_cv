import search from "../img/search.png"
import home from "../img/home.png"
import userImg from "../img/user.png"
import userService from "../api/api.user.service";
import React, { useEffect, useState } from "react";
import { IUserData } from "../models/users/IUserData";

const Users: React.FC = () => {
  const [users, setUsers] = useState<IUserData[] | undefined>(undefined);
  const [searchFio, setSearchFio] = useState<string>('');
  const [nullSearch, setNullSearch] = useState<string>('');

  const [isLoading, setIsLoading] = useState<boolean>(false)

  useEffect(() => {
    const fetchUsers = async () => {
      setIsLoading(true)
      try {
        const fetchedUsers = await userService.getAllUsers()

        // TODO: ошибка получения списка пользователей 

        setUsers(fetchedUsers);
      } catch (error) {
        console.error('Error fetching users:', error);
      } finally {
        setIsLoading(false)
      }
    };

    fetchUsers();
  }, []);

  const handleSearchFio = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchFio(event.target.value)
    onSearch()
  }

  const filteredUsers = users
    ? users.filter((user) => {
      const fullName = `${user.lastName} ${user.firstName} ${user.patronymic}`;
      const regex = new RegExp(searchFio, 'i');
      return regex.test(fullName);
    })
    : undefined;

  const onSearch = () => {
    setNullSearch(filteredUsers === undefined || filteredUsers.length === 0 ? `Не найден пользователь с ФИО: ${searchFio}` : '')
  }

  return (
    <div className="users">
      <div className="users-header">
        <div className="searching">
          <input type="text" placeholder="Введите ФИО пользователя..." onChange={handleSearchFio}></input>
          <button className="search"><img src={search} className="image" alt="search" onClick={onSearch} /></button>
        </div>
        <a className="home-btn" href="/admin"><img src={home} className="image" alt="Home" /></a>
      </div>
      <hr></hr>
      <div className="users-block">
        <p><b>{isLoading ? "Загрузка пользователей..." : ""}</b></p>
        <p style={{ color: 'red' }}>{nullSearch}</p>
        {filteredUsers ? filteredUsers.map((user: IUserData) => (
          <a key={user.id} className="user" href={`/admin/edit-user?fullname=${user.lastName} ${user.firstName} ${user.patronymic}&username=${user.username}&id=${btoa(user.id)}`}>
            <img src={userImg} className="image" alt="User" />
            {`${user.lastName} ${user.firstName} ${user.patronymic}`}
          </a>
        )) : <p>Данные о пользователях не найдены !</p>}
      </div>
    </div>
  );
}

export default Users;