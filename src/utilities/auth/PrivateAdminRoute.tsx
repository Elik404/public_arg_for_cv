import { Navigate, Outlet, useNavigate } from "react-router-dom";

import { observer } from "mobx-react-lite";
import { useContext, useEffect } from "react";
import { Context } from "../../index";
import cookiesService from "../storages/CookiesService";

const PrivateAdminRoute = () => {

  const navigate = useNavigate()

  const { store } = useContext(Context)
  useEffect(() => {
    console.log('RouteA checkAuth')
    if (localStorage.getItem('token')) {
      store.checkAuth()
      setTimeout(() => {
        const isAuth = Boolean(cookiesService.getCookie('isAuth'));
        console.log(`${isAuth}`)
        if (!isAuth) {
          console.log('no isAuth')
          navigate('/login')
        }
      }, 3000)
    }
  }, [store, navigate])

  const isAuth = Boolean(cookiesService.getCookie('isAuth'));

  if (isAuth) {
    return <Outlet />
  } else {
    return <Navigate to="/login" />;
  }
};

export default observer(PrivateAdminRoute);