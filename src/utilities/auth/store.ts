import { makeAutoObservable } from "mobx";
import authService from "../../api/api.auth.service";
import indexedDBService from "../storages/IndexedDBService";
import { jwtDecode, JwtPayload } from "jwt-decode";
import { IAuthData } from "../../models/authorization/IAuthData";
import cookiesService from "../storages/CookiesService";

interface CustomJwtPayload extends JwtPayload {
  realm_access: {
    roles: string[];
  };
  name: string;
}

export default class AuthStore {
  isAuth = false;
  isAdmin = false;

  constructor() {
    makeAutoObservable(this, {}, { autoBind: true });
  }

  setAuth(bool: boolean) {
    this.isAuth = bool;
    cookiesService.setCookie('isAuth', `${this.isAuth}`)
  }

  setAdmin(bool: boolean) {
    this.isAdmin = bool;
    cookiesService.setCookie('isAdmin', `${this.isAdmin}`)
  }

  async login(username: string, password: string) {
    try {
      const response = await authService.login(username, password)
      const repsonseData: IAuthData = await response.json()
      console.log(`ref: ${repsonseData.refresh_token}`)
      console.log(`acc: ${repsonseData.access_token}`)
      localStorage.setItem("token", repsonseData.access_token);
      const decodedAccessToken: CustomJwtPayload = jwtDecode(repsonseData.access_token);
      var roles: string[] = decodedAccessToken.realm_access.roles
      cookiesService.setCookie('name', decodedAccessToken.name)
      if (roles.indexOf("CEO") !== -1 || roles.indexOf("DIRECTOR") !== -1) {
        this.setAdmin(true)
      }
      this.setAuth(true)
      cookiesService.setCookie('refreshToken', repsonseData.refresh_token)  // TODO: Add HttpOnly
    } catch (error) {
      console.log(`login error: ${error}`);
    }
  }

  async checkAuth() {
    try {
      const response = await authService.getTokens(cookiesService.getCookie('refreshToken'))
      const repsonseData: IAuthData = await response.json()
      if (response.status < 400) {
        localStorage.setItem("token", repsonseData.access_token);
        cookiesService.setCookie('refreshToken', repsonseData.refresh_token)
      }
      else {
        this.logout()
      }
    } catch (error) {
      console.log(`auth error: ${error}`);
      this.logout()
    }
  }

  async logout() {
    try {
      console.log('try logout')
      await cookiesService.clearAllCookies();
      localStorage.clear();
      await indexedDBService.clearDB();
      console.log('logout')
    } catch (error) {
      console.log(`logout error: ${error}`);
    }
  }
}
