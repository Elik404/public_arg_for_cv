import { IPhotoData } from "../../models/photos/IPhotoData";

class IndexedDBService {
    private dbName: string;

    constructor(dbName: string) {
        this.dbName = dbName;
    }

    private async openDB(): Promise<IDBDatabase> {
        return new Promise((resolve, reject) => {
            const request = indexedDB.open(this.dbName, 1);

            request.onupgradeneeded = (event) => {
                const db = (event.target as any).result;
                db.createObjectStore('photos', { keyPath: 'litigationId' });
            };

            request.onsuccess = () => {
                resolve(request.result);
            };

            request.onerror = () => {
                reject(request.error);
            };
        });
    }

    public async savePhotos(litigationId: number, photos: IPhotoData[]): Promise<void> {
        console.log('Start saving')
        const db = await this.openDB();
        const transaction = db.transaction('photos', 'readwrite');
        const store = transaction.objectStore('photos');

        store.put({ litigationId, photos: JSON.stringify(photos) });
        console.log('Photos saved')
    }

    public async getPhotos(litigationId: number): Promise<IPhotoData[] | undefined> {
        const db = await this.openDB();
        const transaction = db.transaction('photos', 'readonly');
        const store = transaction.objectStore('photos');

        return new Promise((resolve, reject) => {
            const request = store.get(litigationId);

            request.onsuccess = () => {
                const result = request.result;
                if (result && result.photos) {

                    resolve(JSON.parse(result.photos));
                } else {
                    resolve(undefined);
                }
            };

            request.onerror = () => {
                reject(request.error);
            };
        });
    }

    public async getPhoto(litigationId: number, photoOrder: number): Promise<IPhotoData | undefined> {
        const db = await this.openDB();
        const transaction = db.transaction('photos', 'readonly');
        const store = transaction.objectStore('photos');

        return new Promise((resolve, reject) => {
            const request = store.get(litigationId);

            request.onsuccess = () => {
                const result = request.result;
                if (result && result.photos) {
                    const fetchedPhotos : IPhotoData[] = JSON.parse(result.photos)
                    var photo : IPhotoData | undefined = undefined;
                    for (let fetchPhoto of fetchedPhotos) {
                        if (fetchPhoto.photoOrder === photoOrder) {
                            photo = fetchPhoto;
                        } 
                    }
                    resolve(photo);
                } else {
                    resolve(undefined);
                }
            };

            request.onerror = () => {
                reject(request.error);
            };
        });
    }

    public async clearDB(): Promise<void> {
        try {
            const db = await this.openDB();
            const transaction = db.transaction('photos', 'readwrite');
            const store = transaction.objectStore('photos');

            store.clear();
            console.log('IndexedDB cleared successfully');
        } catch (error) {
            console.error('Error clearing IndexedDB:', error);
        }
    }
}

const indexedDBService = new IndexedDBService('PhotosCache');

export default indexedDBService;