class CookiesService {
    public getCookie(cookieName: string): string {
        return document.cookie.replace(new RegExp(`(?:(?:^|.*;\\s*)${cookieName}\\s*=\\s*([^;]*).*$)|^.*$`), "$1");
    }

    public setCookie(cookieName: string, value: string): void {
        document.cookie = `${cookieName}=${value}; path=/`
    }

    public async clearAllCookies(): Promise<void> {
        const cookies = document.cookie.split(";");
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i];
            const eqPos = cookie.indexOf("=");
            const name = eqPos > -1 ? cookie.substring(0, eqPos) : cookie;
            document.cookie = name + "=; Max-Age=0; path=/";
        }
    }
}

const cookiesService = new CookiesService();

export default cookiesService;