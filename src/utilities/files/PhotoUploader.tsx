import React, { useState } from 'react';
import photosService from '../../api/api.photos.service';
import indexedDBService from '../storages/IndexedDBService';


interface PhotoUploaderProps {
    onCancel: () => void;
    params: {
        litigationId: number;
        photoOrder: number;
    }
}

const PhotoUploader: React.FC<PhotoUploaderProps> = ({ onCancel, params }) => {
    const [file, setFile] = useState<File | null>()
    const [commentText, setCommentText] = useState<string>('')
    const [imagePreview, setImagePreview] = useState<string | null>(null);
    const [errorMessage, setErrorMessage] = useState<string>('');
    const [color, setColor] = useState<string>('red')

    const [countDown, setCountDown] = useState<string>('')

    const [isLoading, setIsLoading] = useState<boolean>(false)
    const [loadingResultMessage, setLoadingResultMessage] = useState<string>('')

    const handleCommentChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const inputText = event.target.value
        setCommentText(inputText)
    }

    const handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const fileInput = e.target.files?.[0] || null;

        if (fileInput) {
            setErrorMessage('')
            const reader = new FileReader();
            reader.onloadend = async () => {
                try {
                    const img = new Image();
                    img.src = URL.createObjectURL(fileInput);
                    img.onload = () => {
                        const canvas = document.createElement('canvas');
                        const ctx = canvas.getContext('2d')!;

                        console.log(`W: ${img.width} H: ${img.height}`)
                        const newWidth = 468;
                        const newHeight = (newWidth / img.width) * img.height;
                        console.log(`NW: ${newWidth} NH: ${newHeight}`)

                        canvas.width = newWidth;
                        canvas.height = newHeight;

                        ctx.drawImage(img, 0, 0, newWidth, newHeight);

                        (async () => {
                            const compressedImageBlob = await new Promise<Blob | null>((resolve) => {
                                canvas.toBlob((blob) => {
                                    resolve(blob);
                                }, 'image/jpeg');
                            });

                            if (compressedImageBlob) {
                                const compressedImageFile = new File([compressedImageBlob], fileInput.name, { type: 'image/jpeg' });
                                setFile(compressedImageFile);
                                console.log('Successfully compressed');
                            }
                        })();
                    };
                    setImagePreview(URL.createObjectURL(fileInput));
                } catch (error) {
                    console.log('Error compressing photo!')
                    setFile(fileInput);
                }
                setImagePreview(URL.createObjectURL(fileInput));;
            };
            reader.readAsDataURL(fileInput);
        } else {
            setFile(undefined)
            setImagePreview(null);
        }
    };

    const onUpload = async () => {
        if (file) {
            setIsLoading(true)
            setColor('black')
            setLoadingResultMessage('Загрузка...')
            try {
                const response = await photosService.uploadPhoto(params.litigationId, commentText,
                    params.photoOrder, file!)
                if (response.status < 400) {
                    const existPhotos = await photosService.getLitigationPhotos(params.litigationId)
                    if (existPhotos)
                        indexedDBService.savePhotos(params.litigationId, existPhotos)
                    setColor('green')
                    setLoadingResultMessage('Фото успешно загружено!')
                }
                else {
                    setColor('red')
                    setLoadingResultMessage('Ошибка загрузки фото')
                }
            }
            catch (error) {
                setColor('red')
                setLoadingResultMessage('Ошибка загрузки фото')
                console.log(`Error upload photo: ${error}`)
            } finally {
                setCountDown('Переход через 3...')
                setTimeout(() => {
                    setCountDown('Переход через 2...')
                }, 1000)
                setTimeout(() => {
                    setCountDown('Переход через 1...')
                }, 2000)

                setTimeout(() => {
                    setIsLoading(false)
                }, 3000)
                setTimeout(() => {
                    onCancel()
                }, 3500)
            }
            setImagePreview(null)
            setFile(null)

        }
        else {
            setErrorMessage(`Выберите фото`)
        }
    };

    return (
        <div className='upload-form'>
            <label>
                <input type="file" accept="image/*" onChange={handleFileChange} />
                <span>Выберите файл</span>
            </label>
            <h2>Комментарий:</h2>
            <input type="text" placeholder="Введите комментарий..." onChange={handleCommentChange} />
            <h3>Выбранное изображение:</h3>
            {imagePreview && (
                <div>
                    <img
                        src={imagePreview}
                        alt="Выбранное изображение"
                        style={{ maxWidth: '100%', maxHeight: '200px', marginTop: '10px' }}
                    />
                </div>
            )}
            <button className="submit-btn" type="button" onClick={onUpload}>Добавить файл</button>
            <div style={{ color: 'red' }}>{errorMessage}</div>
            <button className="reset-btn" type="button" onClick={onCancel}>Отмена</button>

            {isLoading && (
                <div className="load-modal">
                    <p style={{ color: color }}>{loadingResultMessage}</p>
                    <p style={{ color: 'grey', fontSize: '16px' }}>{countDown}</p>
                </div>
            )}
        </div>
    );
};

export default PhotoUploader;