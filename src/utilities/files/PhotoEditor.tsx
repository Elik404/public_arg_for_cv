import React, { useEffect, useState } from 'react';
import photosService from '../../api/api.photos.service';
import { IPhotoData } from '../../models/photos/IPhotoData';
import indexedDBService from '../storages/IndexedDBService';


interface PhotoEditProps {
    onCancel: () => void;
    params: {
        generalPlanPhotoId: number;
        litigationId: number;
        photoOrder: number;
        comment: string;
    }
}

const PhotoEditor: React.FC<PhotoEditProps> = ({ onCancel, params }) => {
    const [file, setFile] = useState<File | null>()
    const [commentText, setCommentText] = useState<string>('')
    const [imagePreview, setImagePreview] = useState<string | null>(null);
    const [color, setColor] = useState<string>('red')
    const [isDeleteModalVisible, setDeleteModalVisible] = useState(false);
    const [photoToEdit, setPhotoToEdit] = useState<IPhotoData | undefined>(undefined)
    const [errorMessage, setErrorMessage] = useState<string>('');

    const [countDown, setCountDown] = useState<string>('')

    const [isLoading, setIsLoading] = useState<boolean>(false)
    const [loadingResultMessage, setLoadingResultMessage] = useState<string>('')

    const handleCommentChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setErrorMessage('')
        const inputText = event.target.value
        setCommentText(inputText)
    }

    const toggleDeleteModal = () => {
        setDeleteModalVisible(!isDeleteModalVisible);
    };

    const handleDelete = async () => {
        toggleDeleteModal();
    }

    const confirmDelete = async () => {
        toggleDeleteModal();


        setIsLoading(true)
        setColor('black')
        setLoadingResultMessage('Удаление...')
        try {
            const response = await photosService.deletePhoto(params.generalPlanPhotoId)
            if (response.status < 400) {
                const existPhotos = await photosService.getLitigationPhotos(params.litigationId);
                if (existPhotos)
                    indexedDBService.savePhotos(params.litigationId, existPhotos)

                setColor('green')
                setLoadingResultMessage('Фото успешно удалено')
            }
            else {
                setColor('red')
                setLoadingResultMessage('Ошибка удаления фото')
            }
        }
        catch (error) {
            setColor('red')
            setLoadingResultMessage('Ошибка удаления фото')
            console.log(`Error deleting: ${error}`)
        } finally {
            setCountDown('Переход через 3...')
            setTimeout(() => {
                setCountDown('Переход через 2...')
            }, 1000)
            setTimeout(() => {
                setCountDown('Переход через 1...')
            }, 2000)

            setTimeout(() => {
                setIsLoading(false)
            }, 3000)
            setTimeout(() => {
                onCancel()
            }, 3500)
        }
    }

    const handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setErrorMessage('')
        const fileInput = e.target.files?.[0] || null;

        if (fileInput) {
            const reader = new FileReader();
            reader.onloadend = async () => {
                try {
                    const img = new Image();
                    img.src = URL.createObjectURL(fileInput);
                    img.onload = () => {
                        const canvas = document.createElement('canvas');
                        const ctx = canvas.getContext('2d')!;

                        console.log(`W: ${img.width} H: ${img.height}`)
                        const newWidth = 468;
                        const newHeight = (newWidth / img.width) * img.height;
                        console.log(`NW: ${newWidth} NH: ${newHeight}`)

                        canvas.width = newWidth;
                        canvas.height = newHeight;

                        ctx.drawImage(img, 0, 0, newWidth, newHeight);

                        (async () => {
                            const compressedImageBlob = await new Promise<Blob | null>((resolve) => {
                                canvas.toBlob((blob) => {
                                    resolve(blob);
                                }, 'image/jpeg');
                            });

                            if (compressedImageBlob) {
                                const compressedImageFile = new File([compressedImageBlob], fileInput.name, { type: 'image/jpeg' });
                                setFile(compressedImageFile);
                                console.log('Successfully compressed');
                            }
                        })();
                    };
                    setImagePreview(URL.createObjectURL(fileInput));
                } catch (error) {
                    console.log('Error compressing photo!')
                    setFile(fileInput);
                }
                setImagePreview(URL.createObjectURL(fileInput));;
            };
            reader.readAsDataURL(fileInput);
        } else {
            setFile(undefined)
            setImagePreview(null);
        }
    };

    const onUpdate = async () => {
        if (!(commentText !== '' || Boolean(file))) {
            setErrorMessage('Введите комментарий или выберите файл!')
        }
        else {
            setIsLoading(true)
            setColor('black')
            setLoadingResultMessage('Обновление...')

            try {
                const response = await photosService.updatePhoto(params.generalPlanPhotoId, commentText,
                    params.photoOrder, file ? file : undefined)
                if (response.status < 400) {
                    const existPhotos = await photosService.getLitigationPhotos(params.litigationId);
                    if (existPhotos)
                        indexedDBService.savePhotos(params.litigationId, existPhotos)

                    setColor('green')
                    setLoadingResultMessage(`Фото успешно обновлено!`)
                }
                else {
                    setColor('red')
                    setLoadingResultMessage(`Ошибка обновления фото`)
                }
            }
            catch (error) {
                setColor('red')
                setLoadingResultMessage(`Ошибка обновления фото`)
                console.log(`Error update photo: ${error}`)
            } finally {
                setCountDown('Переход через 3...')
                setTimeout(() => {
                    setCountDown('Переход через 2...')
                }, 1000)
                setTimeout(() => {
                    setCountDown('Переход через 1...')
                }, 2000)

                setTimeout(() => {
                    setIsLoading(false)
                }, 3000)
                setTimeout(() => {
                    onCancel()
                }, 3500)
            }
            setImagePreview(null)
            setFile(null)
        }
    }


    useEffect(() => {
        const fetchEditPhoto = async () => {
            // setCommentText(params.comment)
            const photo = await indexedDBService.getPhoto(params.litigationId, params.photoOrder)
            if (photo) {
                setPhotoToEdit(photo)
            }
        };

        fetchEditPhoto();
    }, [params]);

    return (
        <div className='edit-photo-form'>
            {photoToEdit &&
                <img src={`data:${photoToEdit.contentType};base64,${photoToEdit.bytes}`} alt={photoToEdit.comment} />
            }
            <h2>Новый комментарий:</h2>
            <input type="text" value={commentText} placeholder='Введите новый комментарий...' onChange={handleCommentChange} />
            <label>
                <input type="file" accept="image/*" onChange={handleFileChange} />
                <span>Выберите файл</span>
            </label>
            <h3>Изображение для замены:</h3>
            {imagePreview && (
                <div>
                    <img
                        src={imagePreview}
                        alt="Выбранное изображение"
                        style={{ maxWidth: '100%', maxHeight: '200px', marginTop: '10px' }}
                    />
                </div>
            )}
            <button className="update-btn" onClick={onUpdate} type="button">Обновить фото</button>
            <div style={{ color: 'red' }}>{errorMessage}</div>
            <button className="delete-btn" type="button" onClick={handleDelete}>Удалить фото</button>
            <button className="reset-btn" type="button" onClick={onCancel}>Отмена</button>

            {isDeleteModalVisible && (
                <div className="delete-modal">
                    <p>Вы уверены, что хотите удалить <b>Фото {params.photoOrder}</b>?</p>
                    <button className='yes' onClick={confirmDelete}>Да</button>
                    <button className='no' onClick={toggleDeleteModal}>Нет</button>
                </div>
            )}

            {isLoading && (
                <div className="load-modal">
                    <p style={{ color: color }}>{loadingResultMessage}</p>
                    <p style={{ color: 'grey', fontSize: '16px' }}>{countDown}</p>
                </div>
            )}
        </div>
    );
};

export default PhotoEditor;